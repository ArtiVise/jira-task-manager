"use strict";

var express = require('express');
var router = express.Router();
var fs = require('fs');
var JiraApi = require('jira-client');
const _request = require('postman-request');
var config = require('../config.json');

let key;
let cert;
try {
    key = fs.readFileSync(config.certKey);
    cert = fs.readFileSync(config.certPem);
} catch (err) {
    console.error("No found files .key and .pem");
}

function request(uri, options) {
    uri.key = key;
    uri.cert = cert;
    return new Promise((resolve, reject) => {
        _request(uri, options,
            (err, httpResponse) => {
                if (err) {
                    reject(err);
                } else {
                    if (httpResponse.statusCode >= 400) {
                        reject(httpResponse.body);
                    } // for compatibility with request-promise


                    resolve(httpResponse.body);
                }
            });
    });
}

var jira = new JiraApi({
    protocol: 'https',
    host: 'helpdesk.compassluxe.com',
    port: 443,
    apiVersion: '2',
    strictSSL: true,
    request: request
});

router.get('/', function (req, res, next) {
    if (req.query.project && req.query.version) {
        Promise.all([
            getVersion(req.query.project),
            jira.searchJira("project=" + req.query.project + "&fixVersion =" + req.query.version + "&status=RESOLVED"),
            jira.searchJira("project=" + req.query.project + "&fixVersion =" + req.query.version + "&status!=RESOLVED"),
            getDescriptionFromComments(req.query.project, req.query.version)
        ]).then(function (result) {
            let listIssues = [];
            let tasks = [];
            let bugs = [];
            let reviews = [];
            let text = "";
            let newRes = result[0];
            let resultFind = result[1];
            let resultFindNotResolved = result[2];
            let descFromComments = result[3];
            resultFind.issues.forEach(function (item, index, array) {
                let newElement = [];
                newElement.push(item.key);
                newElement.push("https://helpdesk.compassluxe.com/browse/" + item.key);
                switch (item.fields["issuetype"].name) {
                    case "Task":
                        if (item.fields["customfield_13748"]) { // changeLog
                            newElement.push(item.fields["customfield_13748"].split("\n")[0]);
                            newElement.push("#99b898");
                        } else if (item.fields["customfield_10131"] && matchDescription(req.query.project,item.fields["customfield_10131"])) { //Комментарий для тех. писателей
                            newElement.push(matchDescription(req.query.project,item.fields["customfield_10131"]));
                            newElement.push("#feceab");
                        } else {
                            let objComm = descFromComments.find(itemComm => itemComm.id === item.id)
                            if (objComm && objComm.comment) {
                                newElement.push(objComm.comment);
                                newElement.push("#4e89ae");
                            } else {
                                newElement.push(item.key);
                                newElement.push("#e84a5f");
                            }
                        }

                        tasks.push(newElement);
                        break;
                    case "Bug":
                        if (item.fields["customfield_13748"]) { // changeLog
                            newElement.push(item.fields["customfield_13748"].split("\n")[0]);
                            newElement.push("#99b898");
                        } else {
                            let objComm = descFromComments.find(itemComm => itemComm.id === item.id)
                            if (objComm && objComm.comment) {
                                newElement.push(objComm.comment);
                                newElement.push("#4e89ae");
                            } else {
                                newElement.push(item.key);
                                newElement.push("#e84a5f");
                            }
                        }
                        bugs.push(newElement);
                        break;
                    case "Review":
                        newElement.push(item.key);
                        newElement.push("#ebecf1");
                        reviews.push(newElement);
                        break;
                }
            });
            if (resultFindNotResolved.issues && resultFindNotResolved.issues.length > 0) {
                text += "Найдено " + resultFindNotResolved.issues.length + " незакрытых задач!" + "\n";
            }
            if (config.description.title_format) {
                text += config.description.title_format + "\n";
            }
            if (tasks.length > 0) {
                text += "\n" + config.description.tasks_format + "\n";
                listIssues.push(["", "", config.description.tasks])
                tasks.forEach(function (item, index, array) {
                    text += item[2] + "\n";
                    listIssues.push([item[0], item[1], item[2], item[3]])
                });
            }
            if (bugs.length > 0) {
                text += "\n" + config.description.bugs_format + "\n";
                listIssues.push(["", "", config.description.bugs])
                bugs.forEach(function (item, index, array) {
                    text += item[2] + "\n";
                    listIssues.push([item[0], item[1], item[2], item[3]])
                });
            }
            if (reviews.length > 0) {
                text += "\n" + config.description.code_review_format + "\n";
                listIssues.push(["", "", config.description.code_review])
                reviews.forEach(function (item, index, array) {
                    text += item[2] + "\n";
                    listIssues.push([item[0], item[1], item[2], item[3]])
                });
            }
            return [newRes, listIssues, text];
        }).then(function (result) {
            res.render('index', {
                title: 'Jira Task Manager',
                projects: ['ECOMPG', 'ECOMACS','TWIB'],
                selectProject: req.query.project,
                versions: result[0],
                selectVersion: req.query.version,
                results: result[1],
                resultText: result[2]
            });
        }).catch(function (err) {
            res.render('error', {
                message: err.message ? err.message : "",
                error: {
                    status: err.status ? err.status : "",
                    stack: err.stack ? err.stack : ""
                }
            });
            console.error(err);
        });
    } else {
        getVersion(req.query.project? req.query.project : 'ECOMPG')
        .then(function (result) {
            res.render('index', {
                title: 'Jira Task Manager',
                projects: ['ECOMPG', 'ECOMACS','TWIB'],
                selectProject: req.query.project? req.query.project : 'ECOMPG',
                versions: result,
                selectVersion: "0",
                results: [],
                resultText: ""
            });
        }).catch(function (err) {
            res.render('error', {
                message: err.message ? err.message : "",
                error: {
                    status: err.status ? err.status : "",
                    stack: err.stack ? err.stack : ""
                }
            });
            console.error(err);
        });
    }
});

function getVersion(project) {
    return jira.getVersions(project).then(function (result) {
        var newRes;
        switch (project) {
            case "ECOMPG":
                newRes = result.filter(item => item.archived !== true && item.name.startsWith("TWPG"))
                break;
            case "ECOMACS":
                newRes = result.filter(item => item.archived !== true && item.name.startsWith("ACS"))
                break;
            case "TWIB":
                newRes = result.filter(item => item.archived !== true && item.name.startsWith("TWIB"))
                break;
        }
        newRes.sort((x, y) => {
            try {
                let ver1 = x.name.split('.')
                let ver2 = y.name.split('.')
                let maxLenght = Math.max(ver1.length, ver2.length)
                for (let i = 0; i < maxLenght; i++) {
                    if (Number(ver1[i]) < Number(ver2[i])) {
                        return -1;
                    } else if (Number(ver1[i]) > Number(ver2[i])) {
                        return 1;
                    }
                }
            } catch (err) {
                console.log(err);
                return 0;
            }
        });
        newRes.reverse();
        return newRes;
    });
}
async function getDescriptionFromComments(project, version) {
    //project = ECOMPG AND fixVersion = "TWPG 3.1.37.12.1" AND ((issuetype = Bug AND Changelog is EMPTY) or (issuetype = Task AND "Комментарий для технических писателей" is EMPTY AND Changelog is  EMPTY))
    let comments = [];
    let resultFind = await jira.searchJira("project=" + project + "&fixVersion =" + version + "&status=RESOLVED" + " AND ((issuetype = Bug AND Changelog is EMPTY) or (issuetype = Task AND Changelog is  EMPTY))")
    for (let res of resultFind.issues) {
        let comment = await getComment(project, res.id);
        comments.push({id: res.id, comment: comment});
    }
    return comments;
}

async function getComment(project, taskID) {
    let resString
    let result = await jira.getComments(taskID);
    let descVersion = ""
    switch (project) {
        case "ECOMPG":
            descVersion = "ECOMPG";
            break;
        case "ECOMACS":
            descVersion = "ECOMACS";
            break;
        case "TWIB":
            descVersion = "TWIB";
            break;
    }
    let regexp = new RegExp(`\\[.\\]\\(${descVersion}-[\\s\\S]*?$`,`m`);
    let resFind = result.comments.find(item => item.body.match(regexp));
    if (resFind) {
        resString = regexp.exec(resFind.body)[0].replace(/(\r\n|\n|\r)/gm, "");
    }
    return resString;
}

function matchDescription(project, text){
    let resString
    let descVersion = ""
    switch (project) {
        case "ECOMPG":
            descVersion = "ECOMPG";
            break;
        case "ECOMACS":
            descVersion = "ECOMACS";
            break;
        case "TWIB":
            descVersion = "TWIB";
            break;
    }
    let regexp = new RegExp(`\\[.\\]\\(${descVersion}-[\\s\\S]*?$`,`m`);
    if (text && text.match(regexp)) {
        resString = regexp.exec(text)[0].replace(/(\r\n|\n|\r)/gm, "");
    }
    return resString;
}


module.exports = router;
