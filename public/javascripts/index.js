src="/socket.io/socket.io.js"
window.onload = function () {
    var socket = io.connect(
        {
            'forceNew': true,
            'reconnection': true,
            'reconnectionDelay': 5000,
            'reconnectionDelayMax': 10000,
            'reconnectionAttempts': 10
        });
    socket.on('redirect', function (destination) {
        window.location.href = destination;
    });
    socket.on('connect', function () {
        console.log('connect from server');
    });
    socket.on('disconnect', function () {
        console.log('disconnected from server');
    });
    document.getElementById('testButton').onclick = function () {
        socket.emit('start');
    }
}