function onConnection(io, socket) {
    socket.on('connect', function () {
        console.log('connect from server');
    });
    socket.on('disconnect', function () {
        console.log('disconnected from server');
    });
    socket.on('start', function () {
        console.log('start');
    });
}

module.exports = onConnection